package si.zivkovic.api;

@javax.annotation.Generated(value = "si.zivkovic.codegen.ZivkovicGenerator", date = "2017-07-14T21:02:02.472+02:00")
public class ApiException extends Exception {
    private int code;
    public ApiException (int code, String msg) {
        super(msg);
        this.code = code;
    }
}
