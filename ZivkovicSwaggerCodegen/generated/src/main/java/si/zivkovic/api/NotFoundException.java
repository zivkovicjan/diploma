package si.zivkovic.api;

@javax.annotation.Generated(value = "si.zivkovic.codegen.ZivkovicGenerator", date = "2017-07-14T21:02:02.472+02:00")
public class NotFoundException extends ApiException {

    private int code;

    public NotFoundException (int code, String msg) {
        super(code, msg);
        this.code = code;
    }
}
