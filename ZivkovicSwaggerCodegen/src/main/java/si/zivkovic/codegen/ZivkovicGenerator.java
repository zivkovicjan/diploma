package si.zivkovic.codegen;

import io.swagger.codegen.*;
import io.swagger.codegen.languages.AbstractJavaCodegen;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Swagger;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.MapProperty;
import io.swagger.models.properties.Property;

import java.io.File;
import java.util.*;

/**
 * Code generator using spring-boot, hibernate ...
 */
public class ZivkovicGenerator extends AbstractJavaCodegen {

	private static final String API_VERSION = "1.0.0";
	private static final String SPRING_BOOT_LIBRARY = "spring-boot";
	//private static final boolean HIBERNATE_ENABLED = true;

	private String projectFolder = "src" + File.separator + "main";
	private String sourceFolder = projectFolder + File.separator + "java";
	private String resourceFolder = projectFolder + File.separator + "resources";

	private String basePackage;
	private String configPackage;
	private String daoPackage;

	public ZivkovicGenerator(){
		super();
		outputFolder = "generated-code/" + getName();
		templateDir = getName();
		basePackage = "si.zivkovic";
		apiPackage = basePackage + ".api";
		modelPackage = basePackage + ".model";
		configPackage = basePackage + ".configuration";
		daoPackage = basePackage + ".dao";

		setArtifactData();
		addSupportedLibraries();

		additionalProperties.put("basePackage", basePackage);
		additionalProperties.put("configPackage", configPackage);
		additionalProperties.put("daoPackage", daoPackage);
		additionalProperties.put("repositoryPackage", basePackage + ".repository");

		CliOption library = new CliOption(CodegenConstants.LIBRARY, CodegenConstants.LIBRARY_DESC);
		library.setDefault(SPRING_BOOT_LIBRARY);
		library.setEnum(supportedLibraries);
		cliOptions.add(library);
	}

	private void setArtifactData(){
		artifactId = getName();
		artifactDescription = getHelp();
		artifactUrl = "zivkovic.si";
		artifactVersion = API_VERSION;
	}

	private void addSupportedLibraries() {
		supportedLibraries.put(SPRING_BOOT_LIBRARY, "Spring-boot Server application using the SpringFox integration.");
		setLibrary(SPRING_BOOT_LIBRARY);
	}

	@Override
	public void processOpts() {
		super.processOpts();
		// removed as in springCodegen
		apiTestTemplateFiles.clear();
		modelDocTemplateFiles.remove("model_doc.mustache");
		apiDocTemplateFiles.remove("api_doc.mustache");

		addTypeMappings();
		addImportMappings();

		String configPath = (sourceFolder + File.separator + configPackage).replace(".", File.separator);
		String basePath = (sourceFolder + File.separator + basePackage).replace(".", File.separator);
		String apiPath = (sourceFolder + File.separator + apiPackage).replace(".", File.separator);
		String daoPath = (sourceFolder + File.separator + daoPackage).replace(".", File.separator);
		String resourcesPath = resourceFolder.replace(".", File.separator);

		supportingFiles.add(new SupportingFile("pom.mustache", "", "pom.xml"));
		supportingFiles.add(new SupportingFile("README.mustache", "", "README.md"));
		supportingFiles.add(new SupportingFile("application.mustache", resourcesPath, "application.yml"));
		supportingFiles.add(new SupportingFile("swagger2SpringBoot.mustache", basePath, "Swagger2SpringBoot.java"));
		supportingFiles.add(new SupportingFile("RFC3339DateFormat.mustache", basePath, "RFC3339DateFormat.java"));

		apiTemplateFiles.put("apiController.mustache", "Controller.java");
		supportingFiles.add(new SupportingFile("apiException.mustache", apiPath, "ApiException.java"));
		supportingFiles.add(new SupportingFile("apiResponseMessage.mustache", apiPath, "ApiResponseMessage.java"));
		supportingFiles.add(new SupportingFile("notFoundException.mustache", apiPath, "NotFoundException.java"));
		supportingFiles.add(new SupportingFile("apiOriginFilter.mustache", apiPath, "ApiOriginFilter.java"));
		supportingFiles.add(new SupportingFile("swaggerDocumentationConfig.mustache", configPath, "SwaggerDocumentationConfig.java"));

		// Add Hibernate specific files.
		//supportingFiles.add(new SupportingFile("abstractDAO.mustache", daoPath, "AbstractDAO.java"));
		//supportingFiles.add(new SupportingFile("hibernateConfiguration.mustache", configPath, "HibernateConfiguration.java"));

		// Jpa repository files.
		modelTemplateFiles.put("customRepository.mustache", "CustomRepository.java");
		modelTemplateFiles.put("repositoryImpl.mustache", "RepositoryImpl.java");
		modelTemplateFiles.put("repository.mustache", "Repository.java");

		// Spring mvc configurer
		supportingFiles.add(new SupportingFile("mvcConfigurer.mustache", configPath, "WebMvcConfigurer.java"));

		addJava8Properties();
	}

	private void addTypeMappings(){
		// is this required?
		typeMapping.put("file", "Resource");
	}

	private void addImportMappings(){
		// is this required?
		importMapping.put("Resource", "org.springframework.core.io.Resource");

		// Hibernate specific mappings.
		importMapping.put("Entity", "javax.persistance.Entity");
		importMapping.put("Table", "javax.persistance.Table");
		importMapping.put("Column", "javax.persistance.Column");
		importMapping.put("Id", "javax.persistance.Id");
		importMapping.put("GeneratedValue", "javax.persistance.GeneratedValue");
		importMapping.put("GenerationType", "javax.persistance.GenerationType");
	}

	private void addJava8Properties(){
		additionalProperties.put("javaVersion", "1.8");
		additionalProperties.put("jdk8", "true");
		typeMapping.put("date", "LocalDate");
		typeMapping.put("DateTime", "OffsetDateTime");
		importMapping.put("LocalDate", "java.time.LocalDate");
		importMapping.put("OffsetDateTime", "java.time.OffsetDateTime");
	}

	@Override
	public String toModelFilename(String name) {
		return super.toModelFilename(name);
	}




/*
	-----------------------------------------------------------------------------------------
	------------------------------------ COPIED METHODS -------------------------------------
	-----------------------------------------------------------------------------------------
	 */

	@Override
	public void addOperationToGroup(String tag, String resourcePath, Operation operation, CodegenOperation co, Map<String, List<CodegenOperation>> operations) {
		if(library.equals(SPRING_BOOT_LIBRARY)) {
			String basePath = resourcePath;
			if (basePath.startsWith("/")) {
				basePath = basePath.substring(1);
			}
			int pos = basePath.indexOf("/");
			if (pos > 0) {
				basePath = basePath.substring(0, pos);
			}

			if (basePath.equals("")) {
				basePath = "default";
			} else {
				co.subresourceOperation = !co.path.isEmpty();
			}
			List<CodegenOperation> opList = operations.get(basePath);
			if (opList == null) {
				opList = new ArrayList<CodegenOperation>();
				operations.put(basePath, opList);
			}
			opList.add(co);
			co.baseName = basePath;
		} else {
			super.addOperationToGroup(tag, resourcePath, operation, co, operations);
		}
	}

	@Override
	public void preprocessSwagger(Swagger swagger) {
		super.preprocessSwagger(swagger);
		if ("/".equals(swagger.getBasePath())) {
			swagger.setBasePath("");
		}

		/*
		if(!additionalProperties.containsKey(TITLE)) {
			// From the title, compute a reasonable name for the package and the API
			String title = swagger.getInfo().getTitle();

			// Drop any API suffix
			if (title != null) {
				title = title.trim().replace(" ", "-");
				if (title.toUpperCase().endsWith("API")) {
					title = title.substring(0, title.length() - 3);
				}

				this.title = camelize(sanitizeName(title), true);
			}
			additionalProperties.put(TITLE, this.title);
		}*/

		String host = swagger.getHost();
		String port = "8080";
		if (host != null) {
			String[] parts = host.split(":");
			if (parts.length > 1) {
				port = parts[1];
			}
		}

		this.additionalProperties.put("serverPort", port);
		if (swagger.getPaths() != null) {
			for (String pathname : swagger.getPaths().keySet()) {
				Path path = swagger.getPath(pathname);
				if (path.getOperations() != null) {
					for (Operation operation : path.getOperations()) {
						if (operation.getTags() != null) {
							List<Map<String, String>> tags = new ArrayList<Map<String, String>>();
							for (String tag : operation.getTags()) {
								Map<String, String> value = new HashMap<String, String>();
								value.put("tag", tag);
								value.put("hasMore", "true");
								tags.add(value);
							}
							if (tags.size() > 0) {
								tags.get(tags.size() - 1).remove("hasMore");
							}
							if (operation.getTags().size() > 0) {
								String tag = operation.getTags().get(0);
								operation.setTags(Arrays.asList(tag));
							}
							operation.setVendorExtension("x-tags", tags);
						}
					}
				}
			}
		}
	}

	@Override
	public Map<String, Object> postProcessOperations(Map<String, Object> objs) {
		Map<String, Object> operations = (Map<String, Object>) objs.get("operations");
		if (operations != null) {
			List<CodegenOperation> ops = (List<CodegenOperation>) operations.get("operation");
			for (CodegenOperation operation : ops) {
				List<CodegenResponse> responses = operation.responses;
				if (responses != null) {
					for (CodegenResponse resp : responses) {
						if ("0".equals(resp.code)) {
							resp.code = "200";
						}
					}
				}

				if (operation.returnType == null) {
					operation.returnType = "Void";
				} else if (operation.returnType.startsWith("List")) {
					String rt = operation.returnType;
					int end = rt.lastIndexOf(">");
					if (end > 0) {
						operation.returnType = rt.substring("List<".length(), end).trim();
						operation.returnContainer = "List";
					}
				} else if (operation.returnType.startsWith("Map")) {
					String rt = operation.returnType;
					int end = rt.lastIndexOf(">");
					if (end > 0) {
						operation.returnType = rt.substring("Map<".length(), end).split(",")[1].trim();
						operation.returnContainer = "Map";
					}
				} else if (operation.returnType.startsWith("Set")) {
					String rt = operation.returnType;
					int end = rt.lastIndexOf(">");
					if (end > 0) {
						operation.returnType = rt.substring("Set<".length(), end).trim();
						operation.returnContainer = "Set";
					}
				}
			}
		}

		return objs;
	}

	/**
	 * This method removes header parameters from the list of parameters and also
	 * corrects last allParams hasMore state.
	 * @param allParams list of all parameters
	 */
	private void removeHeadersFromAllParams(List<CodegenParameter> allParams) {
		if(allParams.isEmpty()){
			return;
		}
		final ArrayList<CodegenParameter> copy = new ArrayList<>(allParams);
		allParams.clear();

		for(CodegenParameter p : copy){
			if(!p.isHeaderParam){
				allParams.add(p);
			}
		}
		allParams.get(allParams.size()-1).hasMore =false;
	}

	@Override
	public Map<String, Object> postProcessSupportingFileData(Map<String, Object> objs) {
		/*
		if(library.equals(SPRING_CLOUD_LIBRARY)) {
			List<CodegenSecurity> authMethods = (List<CodegenSecurity>) objs.get("authMethods");
			if (authMethods != null) {
				for (CodegenSecurity authMethod : authMethods) {
					authMethod.name = camelize(sanitizeName(authMethod.name), true);
				}
			}
		}*/
		return objs;
	}

	@Override
	public void setParameterExampleValue(CodegenParameter p) {
		String type = p.baseType;
		if (type == null) {
			type = p.dataType;
		}

		if ("File".equals(type)) {
			String example;

			if (p.defaultValue == null) {
				example = p.example;
			} else {
				example = p.defaultValue;
			}

			if (example == null) {
				example = "/path/to/file";
			}
			example = "new org.springframework.core.io.FileSystemResource(new java.io.File(\"" + escapeText(example) + "\"))";
			p.example = example;
		} else {
			super.setParameterExampleValue(p);
		}
	}

	private void setHibernateImports(CodegenModel model){
		model.imports.add("Entity");
		model.imports.add("Table");
		model.imports.add("Id");
		model.imports.add("GeneratedValue");
		model.imports.add("GenerationType");
		model.imports.add("Column");
	}

	@Override
	public void postProcessModelProperty(CodegenModel model, CodegenProperty property) {
		super.postProcessModelProperty(model, property);

		setHibernateImports(model);

		if ("null".equals(property.example)) {
			property.example = null;
		}

		//Add imports for Jackson
		if (!Boolean.TRUE.equals(model.isEnum)) {
			model.imports.add("JsonProperty");

			if (Boolean.TRUE.equals(model.hasEnums)) {
				model.imports.add("JsonValue");
			}
		} else { // enum class
			//Needed imports for Jackson's JsonCreator
			if (additionalProperties.containsKey("jackson")) {
				model.imports.add("JsonCreator");
			}
		}
	}

	@Override
	public Map<String, Object> postProcessModelsEnum(Map<String, Object> objs) {
		objs = super.postProcessModelsEnum(objs);

		//Add imports for Jackson
		List<Map<String, String>> imports = (List<Map<String, String>>)objs.get("imports");
		List<Object> models = (List<Object>) objs.get("models");
		for (Object _mo : models) {
			Map<String, Object> mo = (Map<String, Object>) _mo;
			CodegenModel cm = (CodegenModel) mo.get("model");
			// for enum model
			if (Boolean.TRUE.equals(cm.isEnum) && cm.allowableValues != null) {
				cm.imports.add(importMapping.get("JsonValue"));
				Map<String, String> item = new HashMap<String, String>();
				item.put("import", importMapping.get("JsonValue"));
				imports.add(item);
			}
		}

		return objs;
	}















	/*
	-----------------------------------------------------------------------------------------
	--------------------------------- PRE GENERATED METHODS ---------------------------------
	-----------------------------------------------------------------------------------------
	 */
	/*
	public ZivkovicGenerator() {
		super();
		outputFolder = "generated-code/" + getName();

		/**
		 * Models.  You can write model files using the modelTemplateFiles map.
		 * if you want to create one template for file, you can do so here.
		 * for multiple files for model, just put another entry in the `modelTemplateFiles` with
		 * a different extension
		 *
		modelTemplateFiles.put(
				"model.mustache", // the template to use
				".sample");       // the extension for each file to write

		/**
		 * Api classes.  You can write classes for each Api file with the apiTemplateFiles map.
		 * as with models, add multiple entries with different extensions for multiple files per
		 * class
		 *
		apiTemplateFiles.put(
				"example_help.mustache",   // the template to use
				".sample");       // the extension for each file to write

		/**
		 * Template Location.  This is the location which templates will be read from.  The generator
		 * will use the resource stream to attempt to read the templates.
		 *
		templateDir = getName();

		/**
		 * Api Package.  Optional, if needed, this can be used in templates
		 *
		apiPackage = "io.swagger.client.api";

		/**
		 * Model Package.  Optional, if needed, this can be used in templates
		 *
		modelPackage = "io.swagger.client.model";

		/**
		 * Reserved words.  Override this with reserved words specific to your language
		 *
		reservedWords = new HashSet<String>(
				Arrays.asList(
						"sample1",  // replace with static values
						"sample2")
		);

		/**
		 * Additional Properties.  These values can be passed to the templates and
		 * are available in models, apis, and supporting files
		 *
		additionalProperties.put("apiVersion", apiVersion);

		/**
		 * Supporting Files.  You can write single files for the generator with the
		 * entire object tree available.  If the input file has a suffix of `.mustache
		 * it will be processed by the template engine.  Otherwise, it will be copied
		 *
		supportingFiles.add(new SupportingFile("myFile.mustache",   // the input template or file
				"",                                                       // the destination folder, relative `outputFolder`
				"myFile.sample")                                          // the output file
		);

		/**
		 * Language Specific Primitives.  These types will not trigger imports by
		 * the client generator
		 *
		languageSpecificPrimitives = new HashSet<String>(
				Arrays.asList(
						"Type1",      // replace these with your types
						"Type2")
		);
	}*/

	/**
	 * Escapes a reserved word as defined in the `reservedWords` array. Handle escaping
	 * those terms here.  This logic is only called if a variable matches the reseved words
	 *
	 * @return the escaped term
	 */
	@Override
	public String escapeReservedWord(String name) {
		return "_" + name;  // add an underscore to the name
	}

	/**
	 * Location to write model files.  You can use the modelPackage() as defined when the class is
	 * instantiated
	 */
	public String modelFileFolder() {
		return outputFolder + "/" + sourceFolder + "/" + modelPackage().replace('.', File.separatorChar);
	}

	/**
	 * Location to write api files.  You can use the apiPackage() as defined when the class is
	 * instantiated
	 */
	@Override
	public String apiFileFolder() {
		return outputFolder + "/" + sourceFolder + "/" + apiPackage().replace('.', File.separatorChar);
	}

	/**
	 * Optional - type declaration.  This is a String which is used by the templates to instantiate your
	 * types.  There is typically special handling for different property types
	 *
	 * @return a string value used as the `dataType` field for model templates, `returnType` for api templates
	 */
	@Override
	public String getTypeDeclaration(Property p) {
		if (p instanceof ArrayProperty) {
			ArrayProperty ap = (ArrayProperty) p;
			Property inner = ap.getItems();
			return getSwaggerType(p) + "[" + getTypeDeclaration(inner) + "]";
		} else if (p instanceof MapProperty) {
			MapProperty mp = (MapProperty) p;
			Property inner = mp.getAdditionalProperties();
			return getSwaggerType(p) + "[String, " + getTypeDeclaration(inner) + "]";
		}
		return super.getTypeDeclaration(p);
	}

	/**
	 * Optional - swagger type conversion.  This is used to map swagger types in a `Property` into
	 * either language specific types via `typeMapping` or into complex models if there is not a mapping.
	 *
	 * @return a string value of the type or complex model for this property
	 * @see io.swagger.models.properties.Property
	 */
	@Override
	public String getSwaggerType(Property p) {
		return super.getSwaggerType(p);
		// Long conversion error...
		/*
		String swaggerType = super.getSwaggerType(p);
		String type = null;
		if (typeMapping.containsKey(swaggerType)) {
			type = typeMapping.get(swaggerType);
			if (languageSpecificPrimitives.contains(type))
				return toModelName(type);
		} else
			type = swaggerType;
		return toModelName(type);
		*/
	}

	/**
	 * Configures the type of generator.
	 *
	 * @return the CodegenType for this generator
	 * @see io.swagger.codegen.CodegenType
	 */
	public CodegenType getTag() {
		return CodegenType.SERVER;
	}

	/**
	 * Configures a friendly name for the generator.  This will be used by the generator
	 * to select the library with the -l flag.
	 *
	 * @return the friendly name for the generator
	 */
	public String getName() {
		return "zivkovicCodegen";
	}

	/**
	 * Returns human-friendly help for the generator.  Provide the consumer with help
	 * tips, parameters here
	 *
	 * @return A string value for the help message
	 */
	public String getHelp() {
		return "Generates a zivkovicCodegen client library.";
	}
}